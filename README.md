# Lab-04

# Microservices Workshop
Lab 04: Create a CD pipeline with Release Management

---


# Tasks

 - Update the docker-compose files
 
 - Create a release pipeline
 
 - Set the release artifacts
  
 - Configure the environments release process

 - Update the release name format
 
 - Configure the deployment triggers
 
 - Configure a pre-deployment conditions



---


## Create a release pipeline

	
 - Browse to the main builds page and create a new release pipeline:

```
https://dev.azure.com/<your-organization>/microservices-workshop/_release2
```

<img alt="Image 1.1" src="images/task-1.1.png"  width="700">
 
&nbsp;

 - Create an "Empty job":

<img alt="Image 1.2" src="images/task-1.2.png"  width="700">


&nbsp;

## Set the release artifacts


 - Add a "Docker Hub" artifact for each service:

<img alt="Image 2.1" src="images/task-2.1.png"  width="700">

```
Source type: Docker Hub
Service connection: DockerHub
Namespaces: <your-dockerhub-user>
Repository: <service-dockerhub-repository>
Default version: latest
Source alias: <service-name>-artifact
```

&nbsp;

 - Add an "Azure Repositories" artifact to retrieve the docker-compose files:

<img alt="Image 2.2" src="images/task-2.2.png"  width="700">

```
Source type: Azure Repositories
Project: microservices-workshop
Source (repository): microservices-calculator-app
Default branch: master
Source alias: microservices-calculator-app-artifact
```

&nbsp;

## Configure the environments release process


 - Configure the first environment

<img alt="Image 3.1" src="images/task-3.1.png"  width="700">

 - Set the environment name:

<img alt="Image 3.2" src="images/task-3.2.png"  width="700">

 - Create a new Deployment Group Phase (and remove the default agent job phase):

<img alt="Image 3.x" src="images/task-3.x.png"  width="700">

 - Create the first task (remove all the containers in the server):

```
Task: Command Line
Display name: Clean Environment
Script: docker rm -f $(docker ps -a -q)
        docker rmi -f $(docker images -a -q)
Continue on Error: Enabled
``` 

<img alt="Image 3.3" src="images/task-3.3.png"  width="700">

 - Create other task to deploy the application using docker-compose:

```
Task: Docker Compose
Display name: Deploy Application
Docker Compose File: $(System.DefaultWorkingDirectory)/microservices-calculator-app-artifact/docker-compose.yml
Command: up -d --force-recreate
Environment Variables: HOST_IP=<environment-server-ip>
                       DOCKERHUB_USERNAME=leonjalfon1
``` 

<img alt="Image 3.4" src="images/task-3.4.png"  width="700">

 - Come back to the pipeline view:
 

<img alt="Image 3.5" src="images/task-3.5.png"  width="700">


 - Then, clone the stage and configure it to use the appropriate docker-compose file and the appropriate deployment group:
 

<img alt="Image 3.6" src="images/task-3.6.png"  width="700">

 - Repeat the process for each environment

&nbsp;


## Update the release name format


 - Browse to the options hub:

<img alt="Image 4.1" src="images/task-4.1.png"  width="700">

 - Set the release name format:

```
microservices-calculator-app-$(rev:r)
```

<img alt="Image 4.2" src="images/task-4.2.png"  width="700">

&nbsp;


## Configure the deployment trigger


 - Enable continuous deployment trigger for each artifact:

<img alt="Image 5.1" src="images/task-5.1.png"  width="700">

&nbsp;


## Configure a pre-deployment conditions


 - Set a required approval before deploy to production:

<img alt="Image 6.1" src="images/task-6.1.png"  width="700">

&nbsp;

## Set the release pipeline name


 - Set the release pipeline definition name and save it:

```
microservices-calculator-app
```

<img alt="Image 7.1" src="images/task-7.1.png"  width="700">

&nbsp;
